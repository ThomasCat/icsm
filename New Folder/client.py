import select
import socket
import sys

if __name__ == "__main__":
	HOST = ("10.10.6.40", 10000)
	main_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		main_socket.connect(HOST)
		print("Connected to " + HOST[0] + ":" + str(HOST[1]) + '\n')
	except:
		print("Could not connect to " + HOST[0] + ":" + str(HOST[1]) + '\n')
		exit()

	while 1:
		read_buffers = [sys.stdin, main_socket]
		try:
			read_list, write_list, error_list = select.select(read_buffers, [], [])
			for sock in read_list:
				if sock == main_socket:
					data = sock.recv(4096)
					if data:
						data = '\b'+data.decode()
						print('\033[1;33;48m',data,'\033[m') #print('\033[0;30;41m',"CAN'T CONNECT TO NETWORK!",'\033[m')
					else:
						print("Disconnected from server!")
						exit()
				else:
					msg = input()
					main_socket.send(msg.encode())

		except KeyboardInterrupt:
			print("Disconnected from server!")
			exit(1)