#v0.1
#todo
#p2p, server, filesharing, bluetooth

import os, subprocess, platform, sys, time, socketserver, socket, select, threading

PREF_USERNAME="Owais"
PREF_PORT=10000
PREF_ENC="NO"

class OnDeviceServices():
	clienttime = time.strftime("%B %d, %Y | %I:%M %p %Z")

	def clearscreen():
		if platform.system()=="Windows":
			subprocess.Popen("cls", shell=True).communicate()
			os.system('cls')
		else: #Linux and Mac
			print("\033c", end="")
			os.system('clear')

	#To get IP/MAC addresses
	def getAddress(type):
		if type=="localIP":
			try:
				s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
				s.connect(("8.8.8.8", 80))
				localip=s.getsockname()[0]
				s.close()
				return localip
				pass
			except OSError as noConnection:
				print('\033[0;30;41m',"CAN'T CONNECT TO NETWORK!",'\033[m')
				exit()
				raise

		elif type=="btMAC":
			pass

		else:
			print("No address type specified")
			pass

	def coloredPrint():
		print("\033[0;37;40m Normal text\n")
		print("\033[2;37;40m Underlined text\033[0;37;40m \n")
		print("\033[1;37;40m Bright Colour\033[0;37;40m \n")
		print("\033[3;37;40m Negative Colour\033[0;37;40m \n")
		print("\033[5;37;40m Negative Colour\033[0;37;40m\n")
		print("\033[1;37;40m \033[2;37:40m TextColour BlackBackground          TextColour GreyBackground                WhiteText ColouredBackground\033[0;37;40m\n")
		print("\033[1;30;40m Dark Gray      \033[0m 1;30;40m            \033[0;30;47m Black      \033[0m 0;30;47m               \033[0;37;41m Black      \033[0m 0;37;41m")
		print("\033[1;31;40m Bright Red     \033[0m 1;31;40m            \033[0;31;47m Red        \033[0m 0;31;47m               \033[0;37;42m Black      \033[0m 0;37;42m")
		print("\033[1;32;40m Bright Green   \033[0m 1;32;40m            \033[0;32;47m Green      \033[0m 0;32;47m               \033[0;37;43m Black      \033[0m 0;37;43m")
		print("\033[1;33;40m Yellow         \033[0m 1;33;40m            \033[0;33;47m Brown      \033[0m 0;33;47m               \033[0;37;44m Black      \033[0m 0;37;44m")
		print("\033[1;34;40m Bright Blue    \033[0m 1;34;40m            \033[0;34;47m Blue       \033[0m 0;34;47m               \033[0;37;45m Black      \033[0m 0;37;45m")
		print("\033[1;35;40m Bright Magenta \033[0m 1;35;40m            \033[0;35;47m Magenta    \033[0m 0;35;47m               \033[0;37;46m Black      \033[0m 0;37;46m")
		print("\033[1;36;40m Bright Cyan    \033[0m 1;36;40m            \033[0;36;47m Cyan       \033[0m 0;36;47m               \033[0;37;47m Black      \033[0m 0;37;47m")
		print("\033[1;37;40m White          \033[0m 1;37;40m            \033[0;37;40m Light Grey \033[0m 0;37;40m               \033[0;37;48m Black      \033[0m 0;37;48m")

class Comm:
	def commP2P():
		OnDeviceServices.clearscreen()
		myIP=OnDeviceServices.getAddress("localIP")
		print("============================================")
		print("ǁ       ICSM - PEER-TO-PEER LAN CHAT       ǁ")
		print("ǁ------------------------------------------ǁ")
		if myIP.find("127.0.0.1") == 0:
			print("ǁ", '\033[0;30;41m',"Offline!",'\033[m')
			print(	"============================================")
			time.sleep(5)
			OnDeviceServices.clearscreen()
			pass
		else:
			print("ǁ", '\033[0;30;42m',"Online!",'\033[m', "Your IPv4 address: \033[0;30;47m", myIP, '\033[m')
			print(	"============================================")
			IPAddr=input("Enter IPv4 address of receiver: ")
			if IPAddr.find(".") == -1:
				print("\033[5;30;41mINVALID IP ADDRESS!\033[m")
				time.sleep(3)
				OnDeviceServices.clearscreen()
				UserExperience.greetUI()
			else:
				OnDeviceServices.clearscreen()
				ADDRESS = (IPAddr, PREF_PORT)
				txSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				try:
					txSocket.connect(ADDRESS)
					print(IPAddr+" accepted your chat request!")
				except:
					print("Could not connect to " + ADDRESS[0] + ":" + str(ADDRESS[1]) + '\n')
					exit()	

				while 1:
					read_buffers = [sys.stdin, txSocket]
					try:
						read_list, write_list, error_list = select.select(read_buffers, [], [])
						for sock in read_list:
							if sock == txSocket:
								data = sock.recv(4096)
								if data:
									data = '\b'+data.decode()
									print('\033[1;33;48m',data,'\033[m') #print('\033[0;30;41m',"CAN'T CONNECT TO NETWORK!",'\033[m')
								else:
									print("Chat session ended.\n")
									exit()
									break
							else:
								msg = input()
								if msg=="\exit":
									print("Chat session ended." + '\n')
									exit()
									break
								else:
									txSocket.send(msg.encode())
					except KeyboardInterrupt:
						print("Chat session ended.")
						exit(1)

	def commbluetooth():
		OnDeviceServices.clearscreen()
		myIP=OnDeviceServices.getAddress("btMAC")
		print("============================================")
		print("ǁ         ICSM - BLUETOOTH CHAT            ǁ")
		print("ǁ------------------------------------------ǁ")
		print("ǁ  Your MAC is: \033[0;30;47m", myIP, '\033[m')
		print(	"============================================")
		if myIP.find("127.0.0.1") == 0:
			print('\033[0;30;41m',"Offline!",'\033[m')
			time.sleep(4)
			OnDeviceServices.clearscreen()
			pass
		else:
			print('\033[0;30;42m',"Online!",'\033[m')
			rxIPAddr=input("Enter MAC Address of receiver: ")
			pass
		pass

	def fileSharer():
		OnDeviceServices.clearscreen()
		myIP=OnDeviceServices.getAddress("localIP")
		print("============================================")
		print("ǁ           ICSM - FILE SHARING            ǁ")
		print("ǁ------------------------------------------ǁ")
		print("ǁ  Your IPv4 is: \033[0;30;47m", myIP, '\033[m')
		print(	"============================================")
		if myIP.find("127.0.0.1") == 0:
			print('\033[0;30;41m',"Offline!",'\033[m')
			time.sleep(4)
			OnDeviceServices.clearscreen()
			pass
		else:
			print('\033[0;30;42m',"Online!",'\033[m')
			rxIPAddr=input("Enter IPv4 Address of receiver: ")
			pass
		pass
		pass

	def chatroom():
		OnDeviceServices.clearscreen()
		myIP=OnDeviceServices.getAddress("localIP")
		print("============================================")
		print("ǁ               ICSM - CHATROOM             ǁ")
		print("ǁ-------------------------------------------ǁ")
		if myIP.find("127.0.0.1") == 0:
			print("ǁ", '\033[0;30;41m',"Offline!",'\033[m')
			print(	"============================================")
			time.sleep(5)
			OnDeviceServices.clearscreen()
			pass
		else:
			pass
		time.sleep(1)

class UserExperience:
	def settingsUI():
		OnDeviceServices.clearscreen()
		readprefs = open("icsm.py", "rt")
		prefsdata = readprefs.read()
		readprefs.close()
		print("============================================")
		print("ǁ            ICSM - SETTINGS MENU          ǁ")
		print("ǁ------------------------------------------ǁ")
		print(" 1. Username : "+PREF_USERNAME)
		print(" 2. Encryption enabled? : "+PREF_ENC)
		print(" 3. Custom network port : "+str(PREF_PORT))
		print(" 4. Install/uninstall ICSM to system")
		choice = input("――――――――――――――――――――――――――――――――――――――――――――\nEnter option number to change → ")
		if choice=='1':
			oldusernamefield="PREF_USERNAME=\""+PREF_USERNAME+"\""
			newusername=input("Enter new username: ")
			if newusername=="" or len(newusername)>32 or newusername.find("none") == 0 or newusername.find("NONE") == 0:
				newusernamefield="PREF_USERNAME=\""+"NONE"+"\""
				prefsdata = prefsdata.replace(oldusernamefield, newusernamefield)
				writeprefs = open("icsm.py", "wt")
				writeprefs.write(prefsdata)
				writeprefs.close()
				print("Username not saved.")
			else:
				newusernamefield="PREF_USERNAME=\""+newusername+"\""
				prefsdata = prefsdata.replace(oldusernamefield, newusernamefield)
				writeprefs = open("icsm.py", "wt")
				writeprefs.write(prefsdata)
				writeprefs.close()
			print("Saved username. Exiting...")
			time.sleep(3)
			sys.exit()
			pass

		elif choice=='2':
			oldencflagfield="PREF_ENC=\""+PREF_ENC+"\""
			newencflag=input("Should encryption be enabled? (y/n): ")
			if newencflag.find("y") is 0:
				newencflagfield="PREF_ENC=\""+"YES"+"\""
			else:
				newencflagfield="PREF_ENC=\""+"NO"+"\""
			prefsdata = prefsdata.replace(oldencflagfield, newencflagfield)
			writeprefs = open("icsm.py", "wt")
			writeprefs.write(prefsdata)
			writeprefs.close()
			OnDeviceServices.clearscreen()
			print("Saved encryption setting. Exiting...")
			time.sleep(3)
			sys.exit()
			pass

		elif choice=='3':
			oldportfield="PREF_PORT="+str(PREF_PORT)
			newport=int(input("Enter port number (0-65535): "))
			if newport > 65535 or newport == '':
				print("Port number must range from 0 to 65535!")
				exit()
			else:
				newportfield="PREF_PORT="+str(newport)
				prefsdata = prefsdata.replace(oldportfield, newportfield)
				writeprefs = open("icsm.py", "wt")
				writeprefs.write(prefsdata)
				writeprefs.close()
			OnDeviceServices.clearscreen()
			print("Saved port. Exiting...")
			time.sleep(3)
			sys.exit()
			pass

		elif choice=='4':
			if platform.system()=="Windows":
				print("You can pin icsm to the Start Menu or the taskbar.")
				time.sleep(4)
				OnDeviceServices.clearscreen
			elif platform.system()=="posix" or platform.system()=="Linux":#UNIX
				if os.path.isfile("/usr/local/bin/icsm.py") or os.path.isfile("/usr/local/bin/icsm"):
					os.system("echo \"ICSM needs your password to remove files from /usr/local/bin.\";")
					os.system("echo \"Please enter it below.\"")
					os.system("su -c 'rm /usr/local/bin/icsm.py; rm /usr/local/bin/icsm';")
					OnDeviceServices.clearscreen()
					time.sleep(2)
				else:
					os.system("echo \"ICSM needs your password to install files to /usr/local/bin.\";")
					os.system("echo \"Please enter it below.\"")
					os.system("echo '#!/bin/sh' > icsm; echo 'python3 icsm.py' > icsm;")
					os.system("su -c 'rm /usr/local/bin/icsm.py; rm /usr/local/bin/icsm; cp icsm.py /usr/local/bin; cp icsm /usr/local/bin; chmod +x /usr/local/bin/icsm';")
					OnDeviceServices.clearscreen()
					time.sleep(2)
			else:
				print("Unable to determine operating system.")
				time.sleep(3)
			sys.exit()
		
		else:
			OnDeviceServices.clearscreen()
			sys.exit()


	def greetHelp():
		OnDeviceServices.clearscreen()
		print("________________________________\nIMPORTANT CODED SECRET MESSAGES \n________________________________\n© Owais Shaikh 2020\n********************************\n A safe and quick way to communicate\n")
		print("Modes: \n > User\n   - Communications take place between two devices directly\n > Chatroom\n   - Join a central server and talk to other users\n > Files\n   - Share files peer-to-peer between users\n > Bluetooth\n   - Connect two devices using Bluetooth technology\n")
		print()
		choice = input()
		if choice=="": 
			OnDeviceServices.clearscreen() 
			pass
		
	def greetUI(): #to greet the user
		OnDeviceServices.clearscreen()
		print("============================================")
		print("ǁ     IMPORTANT CODED SECRET MESSAGES      ǁ")
		print("ǁ------------------------------------------ǁ")
		print("      ", OnDeviceServices.clienttime)
		print("============================================\n")
		print(" Press 'u' to connect to user")
		print(" Press 'r' to connect to room")
		print(" Press 's' to start chatroom server")
		print(" Preff 'f' to send file")
		#print(" Press 'b' to chat via Bluetooth")
		print(" Press 'p' to configure preferences")
		print(" Press 'h' for help\n")
		choice = input("――――――――――――――――――――――――――――――――――――――――――――\n→ ")

		if choice.lower() == 'u':		
			Comm.commP2P()

		elif choice.lower() == 'r':
			Comm.chatroom()

		elif choice.lower() == 'h':
			UserExperience.greetHelp()

		elif choice.lower() == 'p':
			UserExperience.settingsUI()

		else:
			OnDeviceServices.clearscreen()
			sys.exit()
while 1:
	UserExperience.greetUI()