# Changelog
All notable changes to this project will be documented in this file. 
## [0.1a] 
### Added 
December 2019
- Created project. 
- Added 2 modes of communication - Bluetooth and LAN.
- Added a simple greeter function.
## [0.2a] 
### Added 
January 2020
- Added class files. 
- Added functions to determine time, date and public IP address.
- Implemented plaintext centralized network communication
