import select, socket, sys

def clientUX():
	ADDRESS = ("172.20.10.2", 8000)
	txSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
		txSocket.connect(ADDRESS)
		print(ADDRESS[0]+" accepted your chat request!")
	except:
		print("Could not connect to " + ADDRESS[0] + ":" + str(ADDRESS[1]) + '\n')
		exit()	

	while 1:
		read_buffers = [sys.stdin, txSocket]
		try:
			read_list, write_list, error_list = select.select(read_buffers, [], [])
			for sock in read_list:
				if sock == txSocket:
					data = sock.recv(4096)
					if data:
						data = '\b'+data.decode()
						print('\033[1;33;48m',data,'\033[m') #print('\033[0;30;41m',"CAN'T CONNECT TO NETWORK!",'\033[m')
					else:
						print("Chat session ended.\n")
						exit()
						break
				else:
					msg = input()
					if msg=="\exit":
						print("Chat session ended." + '\n')
						exit()
						break
					else:
						txSocket.send(msg.encode())
		except KeyboardInterrupt:
			print("Chat session ended.")
			exit(1)

clientUX()