#!/bin/sh
echo "ICSM needs your superuser password to modify files in /usr/local/bin.";
echo "Please enter it below."
su -c "rm /usr/local/bin/icsm.py; cp icsm.py /usr/local/bin; echo \"#!/bin/sh\" | tee /usr/local/bin/icsm; echo \"python icsm.py\" | tee /usr/local/bin/icsm; chmod +x /usr/local/bin/icsm";
echo "Done! You can now type 'icsm' in a terminal to run it."
exit;
exit;