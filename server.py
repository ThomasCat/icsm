import socketserver, sys, threading

clientList = []

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
	pass

class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
	def handle(self):
		clientList.append(self.request)
		print(self.client_address[0] + "started a chat session")
		for cli in clientList:
			if cli is not self.request:
				cli.sendall(welcomeMsg.encode())
		while 1:
			data = self.request.recv(4096)
			print(data)
			if data:
				data = data.decode()
				print(data)
				for cli in clientList:
					if cli is not self.request:
						cli.sendall(data.encode())
			else:
				leftmessage = self.client_address[0] + ":" + str(self.client_address[1]) + " left." + '\n'
				print(leftmessage)
				clientList.remove(self.request)
				for cli in clientList:
					cli.sendall(leftmessage.encode())
				break

if __name__ == "__main__":
	HOST = ("172.20.10.2", 10000)

	server = ThreadedTCPServer(HOST, ThreadedTCPRequestHandler)
	server.daemon_threads = True

	server_thread = threading.Thread(target=server.serve_forever)
	server_thread.daemon = True
	server_thread.start()
	print("Server is up." + '\n')
	while True:
		try:
			msg = input()
			for client in clientList:
				client.sendall(msg.encode())

		except KeyboardInterrupt:
			break

	server.shutdown()
	server.server_close()
	print("Server is closed." + '\n')
